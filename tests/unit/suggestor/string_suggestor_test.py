import unittest

from src.suggestor.string_suggestor import StringSuggestor


class StringSuggestorTestCase(unittest.TestCase):
    def test_string_suggestions(self):
        suggestions = StringSuggestor()
        result = suggestions.get_suggestions('SINGH', {'A': 4, 'E': 3, 'I': 1, 'O': 0, 'S': 5})

        self.assertEqual({'SINGH', 'S1NGH', '5INGH', '51NGH'}, result)

    def test_string_suggestions_example_2(self):
        suggestions = StringSuggestor()
        result = suggestions.get_suggestions('PANZAB', {'A': 4, 'E': 3, 'I': 1, 'O': 0, 'S': 5})

        self.assertEqual({'PANZAB', 'P4NZAB', 'PANZ4B', 'P4NZ4B'}, result)

    def test_string_suggestions_example_3(self):
        suggestions = StringSuggestor()
        result = suggestions.get_suggestions('AASS', {'A': 4, 'E': 3, 'I': 1, 'O': 0, 'S': 5})

        self.assertEqual(
            {
                '4455',
                '445S',
                '44S5',
                '44SS',
                '4A55',
                '4A5S',
                '4AS5',
                '4ASS',
                'A455',
                'A45S',
                'A4S5',
                'A4SS',
                'AA55',
                'AA5S',
                'AAS5',
                'AASS',
            },
            result
        )

    def test_string_suggestions_when_none_possible(self):
        suggestions = StringSuggestor()
        result = suggestions.get_suggestions('BCD', {})

        self.assertEqual({'BCD'}, result)

    def test_string_suggestions_replacing_all(self):
        suggestions = StringSuggestor()

        expected = {'AEI', '4EI', '43I', '431', 'A3I', 'A31', 'AE1', '4E1'}
        result = suggestions.get_suggestions('AEI', {'A': 4, 'E': 3, 'I': 1, 'O': 0, 'S': 5})

        self.assertSetEqual(expected, result)

    def test_string_suggestions_grouped(self):
        suggestions = StringSuggestor()

        expected = {
            '5R',
            'FIVER',
            'F1VER',
            'F1V3R',
            'FIV3R',
        }
        result = suggestions.get_suggestions('FIVER', {'A': '4', 'E': '3', 'I': '1', 'O': '0', 'S': '5', 'FIVE': '5'})

        self.assertSetEqual(expected, result)

    def test_same_replacements_do_not_infinite_loop(self):
        suggestions = StringSuggestor()

        expected = {
            'FIVER',
        }
        result = suggestions.get_suggestions('FIVER', {'FIVE': 'FIVE'})

        self.assertSetEqual(expected, result)

