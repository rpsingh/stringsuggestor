from copy import copy


class StringSuggestor:

    def get_suggestions(self, string: str, replacements: dict, suggestions: set = None):
        suggestions = self._make_suggestions(string, replacements)
        count = len(suggestions)
        new_count = 0

        # we got initial conversion, now loop though them all unless we get same count.
        while count != new_count:
            count = len(suggestions)
            for new_string in suggestions.copy():
                suggestions.update(self._make_suggestions(new_string, replacements))

            new_count = len(suggestions)

        # add original to the mix as well
        suggestions.add(string)

        return suggestions

    @staticmethod
    def _make_suggestions(string: str, replacements: dict):
        suggestions = set()
        char_array = list(string)
        i = 0

        # suggestions per character
        while i < len(char_array):
            copy_array = char_array.copy()
            char = copy_array.__getitem__(i)
            replacement = replacements.get(char)
            if replacement is not None:
                copy_array.pop(i)
                copy_array.insert(i, replacement.__str__())
                suggestions.add(''.join(copy_array))

            i += 1

        # suggestions for words
        for i, word in enumerate(replacements):
            if len(word) <= 1:
                continue

            copy_string = copy(string)
            replacement_word = str(replacements[word])
            copy_string = copy_string.replace(word, replacement_word)

            if copy_string is not string:
                suggestions.add(copy_string)

        return suggestions
